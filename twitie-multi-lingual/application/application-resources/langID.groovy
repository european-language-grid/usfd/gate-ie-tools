if (doc.getFeatures().containsKey("lang")) return;

List<Annotation> tweets = gate.Utils.inDocumentOrder(inputAS.get("Tweet"));

for (Annotation tweet in tweets) {
    if (tweet.getFeatures().containsKey("lang")) {
        doc.getFeatures().put("lang", tweet.getFeatures().get("lang"));
        
        return;
    }
}

doc.getFeatures().put("lang","unknown");