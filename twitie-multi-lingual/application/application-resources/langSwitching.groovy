Set<String> tokenizerLanguage = ["de", "en", "fr"];
String defaultTokenizer = "en";

Set<String> gazetteerLanguage = ["de", "en", "fr"];
String defaultGazetteer = "en";

Set<String> hashtagLanguages = ["de", "en", "fr"];
String defaultHashtag = "en";

Set<String> spacyLanguages = ["de", "el", "en", "es", "fr", "it", "pt"];
String defaultSpacy = "unknown"

String lang = doc.getFeatures().get("lang")

doc.getFeatures().put("tokenizer",lang in tokenizerLanguage ? lang : defaultTokenizer);
doc.getFeatures().put("gazetteer",lang in gazetteerLanguage ? lang : defaultGazetteer);
doc.getFeatures().put("hashtag",lang in hashtagLanguages ? lang : defaultHashtag);
doc.getFeatures().put("spacy",lang in spacyLanguages ? lang : defaultSpacy);

System.out.println(doc.getFeatures());