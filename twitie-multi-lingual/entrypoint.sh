#!/bin/sh

if [ "${SPACY_PORT:=8000}" = "80" ]; then
  SPACY_PORT=""
else
  SPACY_PORT=:$SPACY_PORT
fi
export SPACY_PORT

SPACY_PROTOCOL="http${SPACY_SECURE:+s}"
export SPACY_PROTOCOL

# Allow URLs to the various spacy endpoints to be overridden by environment.
# You can either set the whole URL for each with SPACY_XX_URL, or if they all
# follow the standard naming convention then you can set just SPACY_DOMAIN and
# SPACY_PORT.  For example SPACY_DOMAIN=.example.com and SPACY_PORT=8080 would
# use endpoints http://spacy-{lang}.example.com:8080/process

./dockerize-`uname -m` -template /application/application.xgapp:/application/application-subst.xgapp

exec java $JAVA_OPTS -jar /elg/gate-ie-worker-*.jar "$@"
