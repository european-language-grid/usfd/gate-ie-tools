GATE IE Tools
=============

This is a repository to allow us to rebuild a standard set of specific GATE IE tool images whenever the base `gate-ie-worker` code changes.
