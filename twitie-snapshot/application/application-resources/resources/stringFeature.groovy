Set<String> types = new HashSet<String>(["Person","Location","Organization"]);

AnnotationSet annots = inputAS.get(types);

for (Annotation a : annots) {
    if (!a.getFeatures().containsKey("string")) {
        a.getFeatures().put("string", gate.Utils.cleanStringFor(doc,a));
    }
}