// if there is no Tweet annotation found in the input set, create a dummy
// one spanning the whole document

if(!inputAS["Tweet"]) {
  def twtFeatures = [:].toFeatureMap()
  if(doc.sourceUrl?.path != '/dummy') {
    // don't add this for online API calls
    twtFeatures.id_str = doc.name
  }
  outputAS.addAnn(doc.start(), doc.end(), "Tweet", twtFeatures)
}
