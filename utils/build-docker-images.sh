#!/bin/sh

docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY

cd build

for service in * ; do
  (
    cd $service
    docker buildx build --push --pull --platform linux/amd64,linux/arm64/v8 -t "$CI_REGISTRY_IMAGE/$service" .
  )
done
